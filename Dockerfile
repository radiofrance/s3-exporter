FROM golang:1.23.5@sha256:8c10f21bec412f08f73aa7b97ca5ac5f28a39d8a88030ad8a339fd0a781d72b4 AS builder

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux

COPY . /src

RUN cd /src \
    && go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o s3-exporter

FROM debian:bookworm

RUN set -x \
    && apt-get update -y \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd --gid 1664 gopher \
    && useradd --comment 'Go user' --uid 1664 --gid 1664 --shell /bin/sh gopher

COPY --from=builder /src/s3-exporter /usr/local/bin/s3-exporter

RUN chmod +x /usr/local/bin/s3-exporter

USER gopher

ENTRYPOINT [ "/usr/local/bin/s3-exporter" ]
